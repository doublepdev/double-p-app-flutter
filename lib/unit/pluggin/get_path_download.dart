import 'dart:io';
import 'package:flutter/services.dart';

class DownloadsPathProvider {
  static const MethodChannel _channel =
  const MethodChannel('com.onegroupvn.com.getPath');

  static Future<Directory> get downloadsDirectory async {
    final String path = await _channel.invokeMethod('getDownloadsDirectory');
    if (path == null) {
      return null;
    }
    print(Directory(path));
    return Directory(path);
  }
}