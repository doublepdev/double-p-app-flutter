import 'package:flutter/material.dart';

import 'color.dart';

ThemeData themeDataMain = ThemeData(
  primarySwatch: clrThemeFA7A57,
  backgroundColor: clr_white,
  scaffoldBackgroundColor: clr_white,
  unselectedWidgetColor: clr_FA7A57,
  textTheme: TextTheme(),
  accentColor: clr_FA7A57,
  appBarTheme: AppBarTheme(
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: clr_white),
    textTheme: TextTheme(
      caption: TextStyle(color: clr_white),
    ),
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: clr_FA7A57,
    foregroundColor: clr_white,
  ),
);

ThemeData themeDataDark = ThemeData.dark();
