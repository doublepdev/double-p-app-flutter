
import 'package:flutter_app/presenter/home/home_presenter.dart';
import 'package:flutter_app/presenter/login/log_in_presenter.dart';
import 'package:flutter_app/presenter/splash/splash_presenter.dart';
import 'package:flutter_app/screen/home/home_screen.dart';
import 'package:flutter_app/screen/login/login_screen.dart';
import 'package:flutter_app/screen/splash/splash_screen.dart';
import 'package:provider/provider.dart';

final routes = {
  '/': (context) => ChangeNotifierProvider<SplashPresenter>(
    create: (context)=> SplashPresenter(context),
    child: SplashScreen(),
  ),
  '/login': (context) => ChangeNotifierProvider<LoginPresenter>(
        create: (context) => LoginPresenter(),
        child: LoginScreen(),
      ),
  '/page_home': (context) => ChangeNotifierProvider<HomePresenter>(
        create: (context) => HomePresenter(),
        child: HomeScreen(),
      ),

};
