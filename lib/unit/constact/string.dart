

///TODO: Đăng nhập
const txt_account = "Tài khoản";
const txt_hint_account = "Số điện thoại";
const txt_password = "Mật khẩu";
const txt_hint_password = "Nhập mật khẩu";
const txt_save_account = "Nhớ mật khẩu";
const txt_forgot_password = "Quên mật khẩu";
const txt_content_forgot_password =
    "Để lấy lại mật khẩu. Xin vui lòng liên hệ với nhà trường.";
const txt_saved_passwords = "Sử dụng mật khẩu đã lưu";
const txt_btn = "Đăng nhập";
const txt_choose_account = "Chọn tài khoản";
const txt_close = "ĐÓNG LẠI";
const txt_drawer_log_out = "Đăng xuất";
const txt_home_page = "Trường mầm non Onekids";
const txt_contacts_page = "Danh bạ";
const txt_notification_page = "Thông báo";
const txt_account_page = "Tài khoản";
const txt_login_close = 'Đóng';



///TODO: text network
const txt_network_dis_connect_internet = "Không có kết nối Internet.";
const txt_network_not_url = "Sai link url.";
const txt_network_not_connect = "Kết nối thất bại.";
const txt_network_error = "Lỗi ";

///TODO: Dialog New Version
const txt_have_new_version = "Phiên bản mới";
const txt_content_dialog_new_version = "Vui lòng cập nhật phiên bản mới: 1.2.3";
const txt_content_button_update_dialog_new_version = "Cập nhật";
const txt_update = "Sửa";
const txt_delete = "Xoá";
const txt_revoke = "Thu hồi";
const txt_confirm_revoke = "Bạn có chắc muốn thu hồi?";
const txt_message_revoke = "Tin nhắn đã được thu hồi";
const txt_isUpdate = "Đã chỉnh sửa";


///TODO: Dialog No internet
const txt_connect_failed = "Kết nối thất bại";
const txt_content_dialog_no_internet = "Vui lòng kiểm tra đường truyền mạng";
const txt_content_button_refresh = "Tải lại";
const txt_content_button_close = "Đóng";