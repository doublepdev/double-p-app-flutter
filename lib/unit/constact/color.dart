import 'package:flutter/material.dart';

const clrThemeFA7A57 = MaterialColor(
  0xFFFA7A57,
  <int, Color>{
    50: Color(0x05FA7A57),
    100: Color(0x10FA7A57),
    200: Color(0x20FA7A57),
    300: Color(0x30FA7A57),
    400: Color(0x40FA7A57),
    500: Color(0x50FA7A57),
    600: Color(0x60FA7A57),
    700: Color(0x70FA7A57),
    800: Color(0x80FA7A57),
    900: Color(0x90FA7A57),
  },
);

///TODO: Color
const clr_FA7A57 = Color(0xFFFA7A57);
const clr_F5A0A0 = Color(0xFFF5A0A0);
const clr_AFAFAF = Color(0xFFAFAFAF);
const clr_F7CE68 = Color(0xFFF7CE68);
const clr_20_FA7A57 = Color(0x20FA7A57);
const clr_white = Colors.white;
const clr_black = Colors.black;
const clr_464646 = Color(0xFF464646);
const clr_60_464646 = Color(0x60464646);
const clr_FCB0A3 = Color(0xFFFCB0A3);
const clr_FF6F00 = Color(0xFFFF6F00);
const clr_btn_delete_login = Color(0xFFF98787);
const clr_78A1FF = Color(0xFF78A1FF);
const clr_20_78A1FF = Color(0x2078A1FF);
const clr_FA6148 = Color(0xFFFA6148);
const clr_E4ECFF = Color(0xFFE4ECFF);
const clr_A2A2A2 = Color(0xFFA2A2A2);
const clr_50_A2A2A2 = Color(0x50A2A2A2);
const clr_F5F5F5 = Color(0xFFF5F5F5);
const clr_6DE378 = Color(0xFF6DE378);
const clr_7E8EAA = Color(0xFF7E8EAA);
const clr_31_78A1FF = Color(0x3178A1FF);
const clr_D4E1FF = Color(0xFFD4E1FF);
const clr_6F95EC = Color(0xFF6F95EC);
const clr_D5E2FF = Color(0xFFD5E2FF);
const clr_E96F91 = Color(0xFFE96F91);
const clr_FFC4C4 = Color(0xFFFFC4C4);
const clr_E3DFDF = Color(0xFFE3DFDF);
const clr_707070 = Color(0xFF707070);
const clr_ABABAB = Color(0xFFABABAB);
const clr_979797 = Color(0xFF979797);
const clr_E3DDDD = Color(0xFFE3DDDD);
const clr_F98787 = Color(0xFFF98787);
const clr_40_F98787 = Color(0x40F98787);
const clr_B5B1B1 = Color(0xFFB5B1B1);
const clr_A19C9C = Color(0xFFA19C9C);
const clr_80_E2DDDD = Color(0x80E2DDDD);
const clr_50_FA7A57 = Color(0x50FA7A57);
const clr_E4E6AD = Color(0xFFE4E6AD);
const clr_FFEDBF = Color(0xFFFFEDBF);
const clr_F2F2F2 = Color(0xFFF2F2F2);
const clr_DBDBDB = Color(0xFFDBDBDB);
const clr_EEEEEE = Color(0xFFEEEEEE);
const clr_E5E2E2 = Color(0xFFE5E2E2);
const clr_C6C6C9 = Color(0xFFC6C6C9);
const clr_FA8362 = Color(0xFFFA8362);
const clr_EFEFEF = Color(0xFFEFEFEF);
const clr_FFEBE5 = Color(0xFFFFEBE5);
const clr_FCBCAA = Color(0xFFFCBCAA);
const clr_50_FCBCAA = Color(0x50FCBCAA);
const clr_0570B8 = Color(0xFF0570B8);
const clr_57AEFA = Color(0xFF57AEFA);
const clr_F69078 = Color(0xFFF69078);
const clr_676767 = Color(0xFF676767);
const clr_81C8EE = Color(0xFF81C8EE);
const clr_90_E4ECFF = Color(0x90E4ECFF);
const clr_A7A5A5 = Color(0xFFA7A5A5);
const clr_FF8C8C = Color(0xFFFF8C8C);
const clr_54C9EB = Color(0xFF54C9EB);
const clr_AEC5FB = Color(0xFFAEC5FB);
const clr_FFC300 = Color(0xFFFFC300);
const clr_C4DDEE = Color(0xFFC4DDEE);
const clr_FF2626 = Color(0xFFFF2626);
const clr_F8AF9B = Color(0xFFF8AF9B);
const clr_96B3F5 = Color(0xFF96B3F5);
const clr_50_C4DDEE = Color(0x50C4DDEE);

///TODO: Gradient
const clr_btn_log_in = LinearGradient(colors: [
  Color(0x75FA5A2E),
  Color(0xFFFA6148),
]);
const clr_FA6148_75_FA5A2E = LinearGradient(colors: [
  Color(0xFFFA6148),
  Color(0x75FA5A2E),
]);

const clr_FA7A57_FFD500 = LinearGradient(colors: [
  Color(0xFFFA7A57),
  Color(0xFFFFD500),
]);

const clr_AEC5FB_6F95EC = LinearGradient(colors: [
  Color(0xFF6F95EC),
  Color(0xFFAEC5FB),
]);
const clr_FCA4E2_FFD3B0 = LinearGradient(colors: [
  Color(0xFFFCA4E2),
  Color(0xFFFFD3B0),
]);
const clr_FFD3B0_FCA4E2 = LinearGradient(colors: [
  Color(0xFFFFD3B0),
  Color(0xFFFCA4E2),
]);
const clr_78A1FF_78A1FF = LinearGradient(colors: [
  clr_78A1FF,
  clr_78A1FF,
]);
const clr_6F95EC_AEC5FB = LinearGradient(colors: [
  Color(0xFF6F95EC),
  Color(0xFFAEC5FB),
]);
const clr_B4F2DF_4ED7E1 = LinearGradient(
  colors: [
    Color(0xFFB4F2DF),
    Color(0xFF4ED7E1),
  ],
  begin: Alignment.bottomCenter,
  end: Alignment.topCenter,
);
const clr_FEBCA0_FF7482 = LinearGradient(colors: [
  Color(0xFFFEBCA0),
  Color(0xFFFF7482),
]);
const clr_FA7A57_F8AF9B = LinearGradient(colors: [
  Color(0xFFFA7A57),
  Color(0xFFF8AF9B),
]);

const clr_FFFFFF_FFFFFF = LinearGradient(colors: [
  Color(0xFFFFFFF),
  Color(0xFFFFFFF),
]);

const clr_FAACA8_DDD6F3 = LinearGradient(colors: [
  Color(0xFFFAACA8),
  Color(0xFFDDD6F3),
]);

const clr_9FACE6_74EBD5 = LinearGradient(colors: [
  Color(0xFF9FACE6),
  Color(0xFF74EBD5),
]);
