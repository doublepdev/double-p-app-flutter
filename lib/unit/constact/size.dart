import 'package:flutter_app/unit/widget/my_screen.dart';

///TODO:TIme Animation
const time = 200;
///TODO: Size Screen
const WIDTH = 1242;
const HEIGHT = 2688;

///TODO: padding
double padding_1 = MyScreen().setPadding(1.0);
double padding_2 = MyScreen().setPadding(2.0);
double padding_3 = MyScreen().setPadding(3.0);
double padding_4 = MyScreen().setPadding(4.0);
double padding_5 = MyScreen().setPadding(5.0);
double padding_6 = MyScreen().setPadding(6.0);
double padding_7 = MyScreen().setPadding(7.0);
double padding_8 = MyScreen().setPadding(8.0);
double padding_9 = MyScreen().setPadding(9.0);
double padding_10 = MyScreen().setPadding(10.0);
double padding_11 = MyScreen().setPadding(11.0);
double padding_13 = MyScreen().setPadding(13.0);
double padding_15 = MyScreen().setPadding(15.0);
double padding_16 = MyScreen().setPadding(16.0);
double padding_17 = MyScreen().setPadding(17.0);
double padding_18 = MyScreen().setPadding(18.0);
double padding_19 = MyScreen().setPadding(19.0);
double padding_20 = MyScreen().setPadding(20.0);
double padding_25 = MyScreen().setPadding(25.0);
double padding_30 = MyScreen().setPadding(30.0);
double padding_35 = MyScreen().setPadding(35.0);
double padding_40 = MyScreen().setPadding(40.0);
double padding_45 = MyScreen().setPadding(45.0);
double padding_50 = MyScreen().setPadding(50.0);
double padding_60 = MyScreen().setPadding(60.0);
double padding_70 = MyScreen().setPadding(70.0);
double padding_90 = MyScreen().setPadding(90.0);
double padding_100 = MyScreen().setPadding(100.0);
double padding_150 = MyScreen().setPadding(150.0);

///TODO: border
double border_1 = MyScreen().setBorder(1.0);
double border_2 = MyScreen().setBorder(2.0);
double border_3 = MyScreen().setBorder(3.0);
double border_4 = MyScreen().setBorder(4.0);
double border_5 = MyScreen().setBorder(5.0);
double border_6 = MyScreen().setBorder(6.0);
double border_7 = MyScreen().setBorder(7.0);
double border_8 = MyScreen().setBorder(8.0);
double border_9 = MyScreen().setBorder(9.0);
double border_10 = MyScreen().setBorder(10.0);
double border_15 = MyScreen().setBorder(15.0);
double border_20 = MyScreen().setBorder(20.0);
double border_25 = MyScreen().setBorder(25.0);
double border_30 = MyScreen().setBorder(30.0);
double border_40 = MyScreen().setBorder(40.0);
double border_50 = MyScreen().setBorder(50.0);
double border_100 = MyScreen().setBorder(100.0);

///TODO: font
double font_6 = MyScreen().setFontText(4.0);
double font_8 = MyScreen().setFontText(6.0);
double font_9 = MyScreen().setFontText(7.0);
double font_10 = MyScreen().setFontText(8.0);
double font_11 = MyScreen().setFontText(9.0);
double font_12 = MyScreen().setFontText(10.0);
double font_13 = MyScreen().setFontText(11.0);
double font_14 = MyScreen().setFontText(12.0);
double font_15 = MyScreen().setFontText(13.0);
double font_16 = MyScreen().setFontText(14.0);
double font_17 = MyScreen().setFontText(15.0);
double font_18 = MyScreen().setFontText(16.0);
double font_20 = MyScreen().setFontText(18.0);
double font_23 = MyScreen().setFontText(21.0);
double font_25 = MyScreen().setFontText(23.0);
double font_29 = MyScreen().setFontText(27.0);
double font_31 = MyScreen().setFontText(29.0);

///TODO: sizeBox
const size_box_1 = 1.0;
const size_box_2 = 2.0;
const size_box_3 = 3.0;
const size_box_4 = 4.0;
const size_box_5 = 5.0;
const size_box_6 = 6.0;
const size_box_7 = 7.0;
const size_box_8 = 8.0;
const size_box_9 = 9.0;
const size_box_10 = 10.0;
const size_box_15 = 15.0;
const size_box_17 = 17.0;
const size_box_20 = 20.0;
const size_box_30 = 30.0;
const size_box_40 = 40.0;
const size_box_50 = 50.0;
