import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_app/data/data_const/Internal.dart';
import 'package:flutter_app/unit/constact/extension.dart';
import 'package:flutter_app/unit/constact/network/rest_response.dart';
import 'package:flutter_app/unit/constact/string.dart';
import 'package:http/http.dart' as http;


class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  /// thời giam time out
  int timeOutSeconds = 5;

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  ///TODO: GET
  Future<RestResponse> get(String url, {Map<String, String> headers}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    try {
      response = await http.get(url, headers: headers).timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> GET:  TimeOut");
          checkTimeOut = true;
          return null;
        },
      );
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }

    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> GET: DONE");
    return restResponse(response);
  }

  ///TODO: POST
  Future<RestResponse> post(String url, {Map headers, body, encoding}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    try {
      response = await http
          .post(url, body: body, headers: headers, encoding: encoding)
          .timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> POST URL: TimeOut");
          checkTimeOut = true;
          return null;
        },
      );
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }
    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> POST: DONE");
    return restResponse(response);
  }

  ///TODO: POST
  Future<RestResponse> postFormData(String url,
      {Map headers, body, encoding}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    var request = http.MultipartRequest('POST', Uri.parse(url));

    if (headers != null) {
      headers.forEach((key, value) {
        request.headers[key] = value;
      });
    }

    if (body != null) {
      body.forEach((key, value) {
        request.fields[key] = value;
      });
    }

    try {
      var res = await request.send().timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> POST HAVE FILE URL: TimeOut");
          checkTimeOut = true;
          return null;
        },
      );
      response = await http.Response.fromStream(res);
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }
    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> POST: DONE");
    return restResponse(response);
  }

  ///TODO: DELETE
  Future<RestResponse> delete(String url, {Map headers}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    try {
      response = await http.delete(url, headers: headers).timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> DELETE URL: TimeOut");
          checkTimeOut = true;
          return null;
        },
      );
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }

    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> DELETE: DONE");
    return restResponse(response);
  }

  ///TODO: PUT
  Future<RestResponse> put(String url, {Map headers, body, encoding}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    try {
      response = await http
          .put(url, body: body, headers: headers, encoding: encoding)
          .timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> PUT URL: TimeOut");
          checkTimeOut = true;
          return null;
        },
      );
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }

    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> PUT: DONE");
    return restResponse(response);
  }

  ///TODO: POST FILE
  Future<RestResponse> postHaveFile(String url, List<File> listFile,
      {Map headers, Map body, encoding, String paramFileAttachment}) async {
    if (headers == null) {
      Map<String, String> header = {
        "Content-Type": "application/json",
      };
      headers = header;
    }

    print("=====>URL: " + url.toString());
    bool checkTimeOut = false;
    http.Response response;

    RestResponse restResponseCheck = await checkInternetAndUrl(url);
    if (restResponseCheck != null) {
      return restResponseCheck;
    }

    var request = http.MultipartRequest('POST', Uri.parse(url));

    if (headers != null) {
      headers.forEach((key, value) {
        request.headers[key] = value;
      });
    }

    if (body != null) {
      body.forEach((key, value) {
        request.fields[key] = value;
      });
    }

    listFile.forEach((fileAttachment) async {
      request.files.add(await http.MultipartFile.fromPath(
        paramFileAttachment,
        fileAttachment.path,
      ));
    });

    try {
      var res = await request.send().timeout(
        Duration(seconds: timeOutSeconds),
        onTimeout: () {
          print("=====> POST HAVE FILE URL: TimeOut");
          checkTimeOut = true;
          return null;
        },
      );

      response = await http.Response.fromStream(res);
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      checkTimeOut = true;
    }

    if (response == null || checkTimeOut) {
      return RestResponse(
          status: 0, message: txt_network_not_connect, dataJson: "");
    }

    print("=====> POST HAVE FILE URL: DONE");
    return restResponse(response);
  }

  ///TODO: CHECK Internet and Url
  Future<RestResponse> checkInternetAndUrl(String url) async {
    bool hasInternet = await Extension().checkInternet();
    if (!hasInternet) {
      print("=====> GET: Disconnect");
      return RestResponse(
          status: 0, message: txt_network_dis_connect_internet, dataJson: "");
    }

    if (Internal().baseUrl == "" && url.substring(0, 1) == "/") {
      print("=====> POST: Not a url link");
      return RestResponse(
          status: 0, message: txt_network_not_url, dataJson: "");
    }
    return null;
  }

  ///TODO: RestResponse
  RestResponse restResponse(http.Response response) {
    dynamic body;
    try {
      body = jsonDecode(utf8.decode(response.bodyBytes));
    } catch (e) {
      print("FLUTTER ERROR NETWORK: $e");
      // return RestResponse(
      //     status: 0, message: txt_network_not_connect, dataJson: "");
      return RestResponse(
          status: 0, message: "FLUTTER ERROR NETWORK: $e", dataJson: "");
    }

    int status = response.statusCode;
    dynamic data = body["data"] == null ? "" : body["data"];
    String message = body["message"] == null ? "" : body["message"];

    switch (status) {
      case 200: // thành công
        {
          print("=====> RestResponse 200: " + data.toString());
          return RestResponse(status: status, message: message, dataJson: data);
        }
      case 400: // lỗi nhập dữ liệu
        {
          print("=====> RestResponse 400: " + data.toString());
          return RestResponse(status: status, message: message, dataJson: data);
        }
      case 500: // lỗi server
        {
          print("=====> RestResponse 500: " + data.toString());
          return RestResponse(status: status, message: message, dataJson: data);
        }
      case 412: // lỗi không có dữ liệu trong db
        {
          print("=====> RestResponse 412: " + data.toString());
          return RestResponse(status: status, message: message, dataJson: data);
        }
      case 501: // lỗi code
        {
          print("=====> RestResponse 501: " + data.toString());
          return RestResponse(status: status, message: message, dataJson: data);
        }
      default:
        {
          print("=====> RestResponse $status");
          return RestResponse(
              status: 0, message: "$txt_network_error $status", dataJson: "");
        }
    }
  }
}
