import 'package:flutter/material.dart';

class RestResponse {
  int status;
  String message;
  dynamic dataJson;
  dynamic dataModel;

  RestResponse({
    @required this.status,
    @required this.message,
    this.dataJson,
  }) : assert(status != null);
}
