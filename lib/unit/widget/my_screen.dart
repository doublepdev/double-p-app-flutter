import 'package:flutter/material.dart';
import 'package:flutter_app/unit/constact/size.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MyScreen {
  static MyScreen _instance = new MyScreen.internal();

  MyScreen.internal();

  factory MyScreen() => _instance;

  newScreen(BuildContext context, bool allowFontScaling) {
    ScreenUtil.init(context,
        width: WIDTH, height: HEIGHT, allowFontScaling: allowFontScaling);
  }

  double getTab() {
    return ScreenUtil.statusBarHeight;
  }

  double getTabBottom() {
    return ScreenUtil.bottomBarHeight;
  }

  ///TODO: Lấy kích thước màn hình với đơn vị px
  double getWidthPx() {
    return ScreenUtil.screenWidth;
  }

  double getHeightPx() {
    return ScreenUtil.screenHeight;
  }

  ///TODO: Lấy kích thước màn hình với đơn vị dp
  double getWidthDp() {
    return ScreenUtil.screenWidthDp;
  }

  double getHeightDp() {
    return ScreenUtil.screenHeightDp;
  }

  double setBorder(double border) {
    return ScreenUtil().setWidth(border * 4);
  }

  double setPadding(double padding) {
    return ScreenUtil().setWidth(padding * 4);
  }

  double setWidth(double wight) {
    return ScreenUtil().setWidth(wight * 4);
  }

  double setHeight(double height) {
    return ScreenUtil().setHeight(height * 4);
  }

  double setFontText(double font) {
    return ScreenUtil().setSp(font * 4);
  }
}
