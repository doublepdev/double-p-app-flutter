import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyImage {
  Widget getImageSVG(
      {double height,
      Color color,
      double width,
      String url,
      BoxFit boxFit,
      BorderRadius borderRadius}) {
    return Container(
      child: SvgPicture.asset(
        url,
        width: width,
        height: height,
        color: color,
      ),
    );
  }

  Widget getImage(
      {double height,
      double width,
      String url,
      BoxFit boxFit,
      BorderRadius borderRadius}) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius:
            borderRadius != null ? borderRadius : BorderRadius.circular(0),
        image: DecorationImage(
          image: AssetImage(
            url,
          ),
          fit: boxFit != null ? boxFit : BoxFit.cover,
        ),
      ),
    );
  }

  Widget getImageWeb(
      {String urlImage,
      double wight,
      double height,
      BoxFit boxFit,
      BorderRadius borderRadius}) {
    return CachedNetworkImage(
      imageUrl: urlImage,
      imageBuilder: (context, imageProvider) => Container(
        height: height, //50
        width: wight, //50
        decoration: BoxDecoration(
          borderRadius:
              borderRadius != null ? borderRadius : BorderRadius.circular(0),
          image: DecorationImage(
              image: imageProvider,
              fit: boxFit != null ? boxFit : BoxFit.cover),
        ),
      ),
      placeholder: (context, url) => Center(
        child: Container(
          width: 15.0,
          height: 15.0,
          child: CircularProgressIndicator(),
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
