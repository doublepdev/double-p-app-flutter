import 'package:flutter/material.dart';
import 'package:flutter_app/presenter/home/home_presenter.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomePresenter _presenter;
  @override
  Widget build(BuildContext context) {
    return Consumer<HomePresenter>(
      builder: (context,presenter,child){
        _presenter=presenter;
        return Scaffold(
          appBar: AppBar(
            title: Text("Home"),
          ),
          body: Text("Home"),
        );
      },

    );
  }
}
