import 'package:flutter/material.dart';
import 'package:flutter_app/animation/fade_animation.dart';
import 'package:flutter_app/presenter/splash/splash_presenter.dart';
import 'package:flutter_app/unit/constact/color.dart';
import 'package:flutter_app/unit/constact/size.dart';
import 'package:flutter_app/unit/constact/url.dart';
import 'package:flutter_app/unit/widget/my_image.dart';
import 'package:flutter_app/unit/widget/my_screen.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SplashPresenter _presenter;

  @override
  Widget build(BuildContext context) {
    MyScreen().newScreen(context, false);
    return Consumer<SplashPresenter>(
      builder: (context, presenter, child) {
        _presenter=presenter;
        return FadeAnimation(
            1,
            Container(
              padding: EdgeInsets.all(padding_60),
              color: clr_white,
              child: MyImage().getImage(
                boxFit: BoxFit.scaleDown,
                url: png_logo_splash,
              ),
            ));
      },
    );
  }
}
