import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/shared_preferences/shared_preferences.dart';
import 'package:flutter_app/presenter/base_presenter.dart';
import 'package:flutter_app/unit/constact/url.dart';


class SplashPresenter extends BaseViewModel {
  BuildContext _context;
  bool _isLogin;

  ///TODO: Constructor
  SplashPresenter(context) {
    _context = context;
    initPresenter();
  }

  ///TODO: Init Presenter
  initPresenter() {
    _startTime();
  }


  ///TODO: Start get API
  _startTime() async {
    //_isLogin = await Shared().isLoggedIn();


    // /// lấy config nếu ng dùng đã đăng nhập trước
    // if (_isLogin) {
    //   RestResponse restResponseConfig = await APIConfig().getConfigSchool();
    //   if (restResponseConfig.status != 200) {
    //     Extension().showDialogNoInternet(
    //       context: _context,
    //       canClose: false,
    //       onClickButtonReload: _startTime,
    //       content: restResponseConfig.message,
    //     );
    //     return;
    //   }
    // }

    Timer(Duration(seconds: 3), _navigationPage);
  }

  ///TODO: Next page
  Future<void> _navigationPage() async {Navigator.of(_context).pushReplacementNamed(rou_login);
  }
}
