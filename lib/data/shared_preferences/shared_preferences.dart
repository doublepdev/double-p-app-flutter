import 'dart:convert';

class Shared {
  static Shared _instance = new Shared.internal();

  Shared.internal();

  factory Shared() => _instance;
  //
  // ///TODO: get User
  // Future<ModelUserLogin> getUser() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String userPrefs = prefs.getString("userLogin");
  //   if (userPrefs == null || userPrefs.isEmpty) return null;
  //   ModelUserLogin user =
  //       ModelUserLogin.fromMapSharedPreferences(jsonDecode(userPrefs));
  //
  //   return user;
  // }
  //
  // ///TODO: save user login
  // Future saveUserLogin(ModelUserLogin userLogin) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.setString(
  //     "userLogin",
  //     jsonEncode(
  //       userLogin.toMapSharedPreferences(),
  //     ),
  //   );
  //   return 1;
  // }
  //
  //
  // ///TODO: update token
  // Future updateToken(String token) async {
  //   ModelUserLogin userLogin = await getUser();
  //   if (getUserName != null) {
  //     userLogin.setToken(token);
  //     await deleteUsers();
  //     await saveUserLogin(userLogin);
  //   }
  // }
  //
  // ///TODO: check logged
  // Future<bool> isLoggedIn() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String userPrefs = prefs.getString("userLogin");
  //   if (userPrefs == null || userPrefs.isEmpty) return false;
  //
  //   return true;
  // }
  //
  // ///TODO: delete user login
  // Future<int> deleteUsers() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   await prefs.remove("userLogin");
  //   return 1;
  // }
  //
  // ///TODO: get Token
  // Future<String> getToken() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String userPrefs = prefs.getString("userLogin");
  //   if (userPrefs == null || userPrefs.isEmpty) return "";
  //   ModelUserLogin userLogin =
  //       ModelUserLogin.fromMapSharedPreferences(jsonDecode(userPrefs));
  //   return userLogin.token;
  // }
  //
}
